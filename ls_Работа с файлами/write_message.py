filename = 'programming.txt'


def writeFile(filename):
    with open(filename, 'a') as file_object:
        file_object.write("I also love finding meaning in large datasets.\n")
        file_object.write("I love creating apps that can run in a browser.\n")
        print('messages wrote to file')


if __name__ == '__main__':
    writeFile(filename)

