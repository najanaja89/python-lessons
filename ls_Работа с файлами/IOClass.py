import os
import argparse
import file_reader
import write_message

parser = argparse.ArgumentParser(description='path')
parser.add_argument('indir', type=str, help="path")
myArgs = parser.parse_args()


class IOClass:
    def __init__(self, mypath):
        self.path = mypath

    def fileRead(self):
        file_reader.openFile(self.path)

    def writeFile(self):
        write_message.writeFile(self.path)


obj = IOClass(myArgs.indir)
obj.writeFile()
