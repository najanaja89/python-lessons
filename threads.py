import threading
import sys
import time
from concurrent.futures import ThreadPoolExecutor, as_completed
from queue import Queue
import os
from multiprocessing import Process
from threading import Thread
import _thread


def my_function():
    print('Thread started')
    time.sleep(2)
    print('Thread stopped')


my_function()


def my_function_thread():
    print('thread 1')
    time.time(2)
    print('thead 2')


# my_thread = []
# for i in range(5):
#     th = threading.Thread(target=my_function_thread())
#     th.start()
#     my_thread.append(th)
#
# for th in my_thread:
#     th.join()


# def my_custom_thread(i):
#     print(i)
#     time.sleep(2)
#     print(i * 2)
#
#
# for i in range(1, 7):
#     th = threading.Thread(target=my_custom_thread(i))
#     th.start()

# pid = os.getpid()
#
# while True:
#     print(pid, time.time())
#     time.sleep(2)


# class PrintProcess(multiprocessing.Process):
#     def __init__(self, name):
#         super().__init__()
#         self.name = name
#
#     def run(self):
#         print('hello', self.name)
#
#
# p = PrintProcess("Mike")
# p.start()
# p.join()

def decorator(func):
    def wrapper():
        th = threading.Thread(target=func)
        th.start()

    return wrapper


@decorator
def my_function_thread2():
    print('thread 1')
    time.sleep(2)
    print('thead 2')


my_function_thread2()


def f(a):
    return a


result = []

with ThreadPoolExecutor(max_workers=55) as pool:
    for i in range(10):
        result.append(i)

for future in as_completed(result):
    print(future.result())

q = Queue()


def source(n):
    for i in range(1, 1 + n): yield i


def put(n):
    for item in source(n): q.put(item)


def worker():
    while True:
        if q.empty(): sys.exit()
        item = q.get()
        q.task_done()


for x in range(1, 4):
    put(x)
    Thread(target=worker()).start()

    time.sleep(2)

print('Over')
