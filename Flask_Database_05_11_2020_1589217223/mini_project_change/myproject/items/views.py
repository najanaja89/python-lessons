from flask import Blueprint,render_template,redirect,url_for
from myproject import db
from myproject.items.forms import AddForm,DelForm
from myproject.models import Item

items_blueprint = Blueprint('items',
                              __name__,
                              template_folder='templates/items')

@items_blueprint.route('/add', methods=['GET', 'POST'])
def add():
    form = AddForm()

    if form.validate_on_submit():
        name = form.name.data

        # Add new Item to database
        new_item = Item(name)
        db.session.add(new_item)
        db.session.commit()

        return redirect(url_for('items.list'))

    return render_template('add.html',form=form)

@items_blueprint.route('/list')
def list():
    # Grab a list of item from database.
    items = Item.query.all()
    return render_template('list.html', items=items)

@items_blueprint.route('/delete', methods=['GET', 'POST'])
def delete():

    form = DelForm()

    if form.validate_on_submit():
        id = form.id.data
        item = Item.query.get(id)
        db.session.delete(item)
        db.session.commit()

        return redirect(url_for('items.list'))
    return render_template('delete.html',form=form)
