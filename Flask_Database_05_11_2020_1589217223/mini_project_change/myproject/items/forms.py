from flask_wtf import FlaskForm
from wtforms import StringField, IntegerField, SubmitField

class AddForm(FlaskForm):

    name = StringField('Имя')
    submit = SubmitField('Добавить задание, вещь ')

class DelForm(FlaskForm):

    id = IntegerField('ID Для удадения, задания, вещи')
    submit = SubmitField('Удалить')
