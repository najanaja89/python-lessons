from myproject import db

class Item(db.Model):

    __tablename__ = 'items'
    id = db.Column(db.Integer,primary_key = True)
    name = db.Column(db.Text)
    owner = db.relationship('Owner',backref='item',uselist=False)

    def __init__(self,name):
        self.name = name

    def __repr__(self):
        if self.owner:
            return f"Название {self.name} и ответственный (ая) - {self.owner.name}"
        else:
            return f"Название {self.name} и ответственного еще нет"

class Owner(db.Model):

    __tablename__ = 'owners'

    id = db.Column(db.Integer,primary_key= True)
    name = db.Column(db.Text)
    item_id = db.Column(db.Integer,db.ForeignKey('items.id'))

    def __init__(self,name,item_id):
        self.name = name
        self.item_id = item_id

    def __repr__(self):
        return f"Owner Name: {self.name}"
