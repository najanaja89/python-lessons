from flask_wtf import FlaskForm
from wtforms import StringField, IntegerField, SubmitField

class AddForm(FlaskForm):

    name = StringField('Имя ответственного :')
    item_id = IntegerField("Id вещи, задания: ")
    submit = SubmitField('Добавить ответственного')
