from flask import Flask, render_template, session, redirect, url_for
from flask_wtf import FlaskForm
from wtforms import (StringField, BooleanField, DateField,
                     RadioField, SelectField, TextAreaField,
                     TextField, SubmitField)
from wtforms.validators import DataRequired

app = Flask(__name__)

app.config['SECRET_KEY'] = 'mykey'


class SurveyForm(FlaskForm):
  name = StringField("Как вас зовут? ", validators=[DataRequired( )])
  status = BooleanField("Вы программист? ")
  programming_lang = RadioField("Какой язык вам нравится? ",
                                choices=[('one', "Python"), ('two',"Java")])
  framework = SelectField('Выберите фреймворк: ',
                          choices=[('php', 'Laravel'), ('Python', 'Flask')])
  feedback = TextAreaField( )
  submit = SubmitField("Отправить")


@app.route('/', methods=['GET', 'POST'])
def index():
  form = SurveyForm( )
  if form.validate_on_submit( ):
    session["name"] = form.name.data
    session["status"] = form.status.data
    session["programming_lang"] = form.programming_lang.data

    session["feedback"] = form.feedback.data

    return redirect(url_for("success"))

  return render_template('index.html', form=form)


@app.route('/success')
def success():
  return render_template("success.html")


if __name__ == '__main__':
  app.run(debug=True)

