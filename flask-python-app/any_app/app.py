from flask import Flask ,render_template, request

app = Flask(__name__)
app.config['SEND_FILE_MAX_AGE_DEFAULT'] = 0


@app.route('/')
def index():
  return render_template('index.html')

@app.route('/signup')
def signup():
  return render_template('signup.html')

@app.route('/thankyou')
def thankyou():
  username = request.args.get('name')
  lastname = request.args.get('lastname')
  return render_template('thankyou.html', lastname=lastname)


@app.errorhandler(404)
def page_not_found(e):
  return render_template('404.html'),404



if __name__ == '__main__':
  app.run(debug=True)