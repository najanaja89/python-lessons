from flask import Flask

app = Flask(__name__)


@app.route('/')
def index():
  return '<h1>Hello!</h1>'


@app.route('/information')
def info():
  return '<h1>Инфорация!</h1>'


@app.route('/user/<float:number>')  #
def number(number):
  return f"{number}"


@app.route('/user/<string:name>')  #
def user(name):
  int_name = ' '.join(format(ord(x), 'b') for x in name)
  # int_name_map= ''.join(map(bin, bytearray(int_name, encoding='utf8')))
  #res = ''.join(format(i, 'b') for i in bytearray(test_str, encoding ='utf-8'))
  # print(int_name_map)
  return '<h1>User is {}'.format(int_name)




if __name__ == '__main__':
  app.run(debug=True)
