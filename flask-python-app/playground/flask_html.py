from flask import Flask, render_template

app = Flask(__name__)


@app.route('/')
def index():
  name = "SJ"
  list_of_name = list(name)
  dictionary_name = {"name": "Словарь"}
  book_store = ["Пушкин", "Алгоритмизация", "Игра престолов"]
  return render_template('simple.html', name=name, list_of_name=list_of_name,
                         dictionary_name=dictionary_name, prosto_string = 'string',
                         items = ['book1', 'book2'], books = book_store)
if __name__ == '__main__':
  app.run(debug=True)
