import os
import unittest

DB_PATH = './test.db'


class Test01_CreateTestDatabase(unittest.TestCase):
    def test_it(self):
        global Database, db

        from own_orm import Database

        if os.path.exists(DB_PATH):
            os.remove(DB_PATH)

        db = Database(DB_PATH)

        assert db.table == []


class Test02_DefineTables(Test01_CreateTestDatabase):
    def test_it(self):
        super().test_it()
        global Table, Column, ForeignKey
        global Author, Post

        from own_orm import Table, Column, ForeignKey

        class Author(Table):
            name = Column(str)
            number = Column(str)

        class Post(Table):
            title = Column(str)
            published = Column(bool)
            author = Column(Author)

        assert Author.name.type == str
        assert Post.author.table == Author
