import sqlite3


class Database:
    def __init__(self, path):
        self.conn = sqlite3.Connection(path)

    @property
    def table(self):
        return []


class Table:
    pass


class Column:
    def __init__(self, type):
        self.type = type


class ForeignKey:
    def __init__(self, table):
        self.table = table
