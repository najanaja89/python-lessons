import sys
import math


def item_in_keyorder(d):
    for key in sorted(d):
        yield key, d[key]


def quarters(next_quarter=0.0):
    while True:
        yield next_quarter
        next_quarter += 0.25


d = {1: 'a', 2: 'b', 3: 'c', 4: 'd'}
print(item_in_keyorder(d))

# print(quarters())

# result = []
# for x in quarters():
#     result.append(x)
#     print(result)
#     if x >= 1.0:
#         break

result = []
generator = quarters()
while len(result) < 5:
    x = next(generator)
    print(x)
    if abs(x - 0.5) > sys.float_info.epsilon:
        x = generator.send(1.0)
    result.append(x)
    # print(result)

code = '''
def area_of_sphere(r):
    return 4 * math.pi * r ** 2
'''
context = {}

context["math"] = math
exec(code, context)
