class Person:
    def __init__(self, name, jobs, age=None):
        self._name = name
        self.jobs = jobs
        self.age = age

    def info(self):
        return (self.name, self.jobs)

    @property
    def name(self):
        print('Getter')
        return self._name

    @name.setter
    def name(self, name):
        print('Setter')
        self._name = name

    def __repr__(self):
        return 'Class %s, Impl name: %s, Jobs: %s' % (self.__class__, self._name, self.jobs)


class Manager(Person):
    def giveRaise(self, percent, bonus=0.10):
        Person.giveRaise(percent + bonus)


bob = Person('Bob', 'dev')

# print(bob.age)
print(bob.name)

bob.name = 'Bob Smith'

print(bob.name)

print(bob)
