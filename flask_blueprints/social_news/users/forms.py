from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, SubmitField
from wtforms.validators import DataRequired, Email, EqualTo
from wtforms import ValidationError
from flask_wtf.file import FileField, FileAllowed

from social_news.models import User


class LoginForm(FlaskForm):
    email = StringField("Email", validators=[DataRequired(), Email()])
    password = PasswordField('Password', validators=[DataRequired()])
    submit = SubmitField("Login")


class RegistrationForm(FlaskForm):
    email = StringField("Email", validators=[DataRequired(), Email()])
    username = StringField("Login", validators=[DataRequired()])
    password = PasswordField('Password', validators=[DataRequired(), EqualTo('password', message="Password not Equal")])
    pass_confirm = PasswordField("Confirm Password", validators=[DataRequired()])
    submit = SubmitField("Register")

    def check_email(self, field):
        if User.query.filter_by(email=field.data).first():
            raise ValidationError("Email exists")

    def check_username(self, field):
        if User.query.filter_by(username=field.data).first():
            raise ValidationError("Username exists")


class UpdateUserForm(FlaskForm):
    email = StringField("Email", validators=[DataRequired(), Email()])
    username = StringField("Login", validators=[DataRequired()])
    picture = FileField("Upload Picture", validators=[FileAllowed(['jpg', 'png', 'jpeg'])])

    def check_email(self, field):
        if User.query.filter_by(email=field.data).first():
            raise ValidationError("Email exists")

    def check_username(self, field):
        if User.query.filter_by(username=field.data).first():
            raise ValidationError("Username exists")