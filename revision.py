import collections


class Food:
    pass


class Meat(Food):
    pass


class Milk(Food):
    pass


class Flour(Food):
    pass


class Rabbit(Meat):
    pass


class Pork(Meat):
    pass


class Pasty(Milk, Flour):
    pass


class Pie(Rabbit, Pork, Pasty):
    pass


print(Pie.mro())


# class Tokenizer:
#     def __getinitargs__(self, text):
#         self.text = text
#     def tokens(self, text):
#         return text.split()
#
#
# class WordCounter(Tokenizer):
#     def Count(self, text):
#         return collections.Counter(super().tokens(text))
#
#
# class Vocabulary(Tokenizer):
#     pass
#
# class TextDescriber(Tokenizer, Vocabulary, WordCounter):
#     def __init__(self, text):
#         self.text = text
#
#     def printInfo(self):
#         print(super().Count(self.text))


def MyDecor(function):
    def wrapper(*args):
        func = function(*args)
        decored = "***" + func
        return decored

    return wrapper()


@MyDecor
def MyText(text):
    return text


print(MyText('text'))
