import re
from bs4 import BeautifulSoup

SIMPLE_HTML = '''<html>
<head></head>
<body>
<h1>Это заголовок первого уровня</h1>
<p class="subtitle">Параграф с классом.</p>
<p>Параграф без класса</p>
<ul>
    <li>Пайтон</li>
    <li>Джава</li>
    <li>Си</li>
    <li>Пхп</li>
</ul>
</body>
</html>'''

ITEM_HTML = '''<html><head></head><body>
<li class="col-xs-6 col-sm-4 col-md-3 col-lg-3">
    <article class="product_pod">
            <div class="image_container">
                    <a href="catalogue/a-light-in-the-attic_1000/index.html"><img src="media/cache/2c/da/2cdad67c44b002e7ead0cc35693c0e8b.jpg" alt="A Light in the Attic" class="thumbnail"></a>
            </div>
                <p class="star-rating Three">
                    <i class="icon-star"></i>
                    <i class="icon-star"></i>
                    <i class="icon-star"></i>
                    <i class="icon-star"></i>
                    <i class="icon-star"></i>
                </p>
            <h3><a href="catalogue/a-light-in-the-attic_1000/index.html" title="A Light in the Attic">A Light in the ...</a></h3>
            <div class="product_price">
        <p class="price_color">£51.77</p>
<p class="instock availability">
    <i class="icon-ok"></i>
        In stock
</p>
    <form>
        <button type="submit" class="btn btn-primary btn-block" data-loading-text="Adding...">Add to basket</button>
    </form>
            </div>
    </article>
</li>
</body></html>
'''

soup = BeautifulSoup(SIMPLE_HTML, 'html.parser')


def find_title():
    print(soup.find('h1').string)


def find_li():
    list_items = soup.find_all('li')
    print(list_items)


def p_with_class():
    p = soup.find('p', {'class': "subtitle"}).string
    print(p)


def p_without_class():
    p = soup.find('p', {'class': ''}).string
    print(p)


p_with_class()
find_title()
find_li()
p_without_class()

class ParseLocator:
    NAME_LOCATOR = 'article.product_pod h3 a'
    lINK_LOCATOR = 'article.product_pod h3 a'
    RATE_LOCATOR = 'article.product_pod p.star-rating'
    PRICE_LOCATOR = 'article.product_pod p.price_color'



class ParseItem:


    def __init__(self, page):
        self.soup = BeautifulSoup(page, 'html.parser')

    @property
    def name(self):
        locator = ParseLocator.NAME_LOCATOR
        item_name = self.soup.select_one(locator).attrs['title']
        return item_name

    @property
    def link(self):
        locator = ParseLocator.lINK_LOCATOR
        item_link = self.soup.select_one(locator).attrs['href']
        return item_link

    @property
    def price(self):
        locator = ParseLocator.PRICE_LOCATOR
        item_price = self.soup.select_one(locator).string
        pattern = '£([0-9]+\.[0-9]+)'
        exp = re.search(pattern, item_price)
        return float(exp.group(1)) * 15

    @property
    def rating(self):
        locator = ParseLocator.RATE_LOCATOR
        star_rating = self.soup.select_one(locator)
        classes = star_rating.attrs['class']
        number = list(filter(lambda x: x == 'Three', classes))
        return number


parseItem = ParseItem(ITEM_HTML)
parseItem.name
parseItem.link
parseItem.rating
