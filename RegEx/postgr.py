import psycopg2


try:
    connection = psycopg2.connect(
        host='localhost',
        database='testPostgresDb',
        user='postgres',
        password=""
    )
    print('Connected Successful')
    cursor = connection.cursor()
    cursor.execute('SELECT * FROM customers')
    row = cursor.fetchall()
    print(row)

except (Exception, psycopg2.Error) as error:
    print('Error to connection to db', error)
