from flask import Flask
from binascii import hexlify

app = Flask(__name__)


@app.route('/')
def index():
    return '<h1> Hello <h1>'


@app.route('/user/<name>')  #передаем string
def get_name(name):
    bytes = ''.join(format(ord(x), 'b') for x in name)
    return f'<h1>{bytes}<h1>'


@app.route('/number/<int:num>') #передаем аргументы
def get_num(num):
    num = num + 2
    return f'{num}'


if __name__ == '__main__':
    app.run(debug=True)
