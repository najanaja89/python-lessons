from flask import Flask, render_template, request
from binascii import hexlify

# по умолчанию ищет файлы html в папке templates
app = Flask(__name__)

app.config["SEND_FILE_MAX_AGE_DEFAULT"] = 0 #Запрещает сохранять стили на стороне клиента в кеше
app.debug = True


@app.route('/')
def home():
    username = 'NajaNaja'
    list_username = list(username)
    dict_username = dict(username='Ruslan')
    movies = ['Hyperion', 'Neroumancer', 'Count Zero']
    return render_template('index.html', username=username, list_username=list_username,
                           dict_username=dict_username, movies_list=movies)


@app.route('/info')
def info():
    return render_template('info.html')


@app.route('/signup', endpoint='signup')
def signup():
    return render_template('signup.html')


@app.route('/thankyou', endpoint="thankyou") #endpoint псевдоним для пути /thankyou
def thankyou():
    name = request.args.get('name')
    lastname = request.args.get('lastname')

    return render_template('thankyou.html', name=name, lastname=lastname)


if __name__ == '__main__':
    app.run()
