from flask import Flask

app = Flask(__name__)


@app.route('/home')
def home():
    return 'Hello World'


@app.route('/home/<int:first_id>')
def sum_two_number():
    num = 5 + 5
    return (num,)


if __name__ == '__main__':
    app.run(debug=True)
