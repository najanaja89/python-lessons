from utils import database

USER_CHOICE = """
    Enter :
    'a' - add a book
    'l' - list books
    'r' - mark as read
    'd' - delete a book
    'q' - to quit
"""


def menu():
    database.create_book_table()
    user_input = input(USER_CHOICE)
    while user_input != 'q':
        if user_input == 'a':
            name = input("Enter the name of the book ")
            author = input('Enter the author of the book ')
            database.add_book(name, author)
            user_input = input(USER_CHOICE)
        elif user_input == "l":
            list_books()
            user_input = input(USER_CHOICE)
        elif user_input == "r":
            mark_book_as_read()
            user_input = input(USER_CHOICE)
        elif user_input == 'd':
            delete_prompt_book()
            user_input = input(USER_CHOICE)


def list_books():
    books = database.get_all_books()
    for book in books:
        print('Name is {0}, author is {1}, read: {2}'.format(book['name'], book['author'], book['read']))


def mark_book_as_read():
    name = input('Enter name of the book to mark for read: ')
    database.update_book(name)


def delete_prompt_book():
    name = input('Enter name of the book to delete: ')
    database.delete_book(name)


if __name__ == '__main__':
    menu()
