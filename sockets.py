import socket

exec('print(dir())')

SERVER_ADDRESS = ('localhost', 8787)

server_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
server_socket.bind(SERVER_ADDRESS)
server_socket.listen(10)
print('server running')

while True:
    connection, address = server_socket.accept()
    print('new connection '.format(address=address))
    data = connection.recv(1024)
    print(str(data))

    connection.send(bytes('Hello from server', encoding='UTF-8'))

    connection.close()



