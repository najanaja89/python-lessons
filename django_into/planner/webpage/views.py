from django.shortcuts import render, HttpResponse, get_list_or_404
import datetime
from meetings.models import Meeting, Room

# Create your views here.
def welcome(request):
    meetings = get_list_or_404(Meeting)
    return render(request, "webpage/welcome.html", {'meetings' : meetings})


def date(request):
    return HttpResponse(f"{datetime.datetime.today()}")





