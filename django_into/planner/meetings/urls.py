from django.contrib import admin
from django.urls import path
from meetings import views


urlpatterns = [
 path('meetings/<int:id>', views.detail, name='detail')
]

