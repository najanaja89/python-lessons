from flask import Flask
import os
from flask_restful import Resource, Api
from flask_jwt import JWT, jwt_required
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from secure_check import authenticate, identity

app = Flask(__name__)
app.config['SECRET_KEY'] = 'mysecret'
api = Api(app)
basedir = os.path.abspath(os.path.dirname(__file__))
app.config['SQLALCHEMY_DATABASE_URI'] ='postgresql://postgres:""@localhost/testPostgresDb'
app.config['SQLALCHEMY_TRACK_MODIFICATION'] = False

db = SQLAlchemy(app)
Migrate(app, db)

api = Api(app)

jwt = JWT(app, authenticate, identity)


##############################################################
class Friend(db.Model):
    name = db.Column(db.String(80), primary_key=True)

    def __init__(self, name):
        self.name = name

    def json(self):
        return {'name': self.name}


##############################################################

class FriendName(Resource):
    def get(self, name):
        friend = Friend.query.filter_by(name=name).first()
        if friend:
            return friend.json()
        else:
            return {'name': None}

    def post(self, name):
        friend = Friend(name=name)
        db.session.add(friend)
        db.session.commit()
        return friend.json()

    def delete(self, name):
        friend = Friend.query.filter_by(name=name).first()
        db.delete(friend)
        db.session.commit()
        return {'response': 'success'}


class AllFriends(Resource):
    @jwt_required()
    def get(self):
        friends = Friend.query.all()
        return [friend.json() for friend in friends]


api.add_resource(AllFriends, '/friends')
api.add_resource(FriendName, '/friend/<string:name>')
# class ApiOne(Resource):


#     def get(self):
#         return {'hello':'world GET'}
#     def post(self):
#         return {'hello': 'world POST'}
#     def put(self):
#         return {'hello': 'world PUT'}

# api.add_resource(ApiOne, '/')


if __name__ == '__main__':
    app.run(debug=True)
