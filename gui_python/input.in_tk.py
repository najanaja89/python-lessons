import tkinter as tk
from tkinter import ttk

def greet():
    print('Hello: ', user_name.get())

root = tk.Tk()
root.title('Greet')
user_name = tk.StringVar()

name_label = ttk.Label(root, text='Your name')
name_label.pack(side='top', padx=10, pady=5)
name_entry = ttk.Entry(root, width=20, textvariable=user_name)
name_entry.pack(side='left')
name_entry.focus()

greet_button = ttk.Button(root, text='Hello', command=greet)
greet_button.pack(side='top', fill='both', expand=True)
root.mainloop()
