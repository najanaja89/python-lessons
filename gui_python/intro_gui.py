import tkinter as tk
from tkinter import ttk

root = tk.Tk()
root.title("My Application")


def say_hello():
    print('Hello')


button1 = ttk.Button(root, text='Hello', command=say_hello)
button1.pack(side='right', fill="x", expand=True) #fill = x,y, both

button_exit  = ttk.Button(root, text='Exit', command=root.destroy)
button_exit.pack(side='top', fill="x", expand=True)
root.mainloop()
