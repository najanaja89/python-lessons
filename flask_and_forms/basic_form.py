from flask import Flask, render_template
from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField

app = Flask(__name__)

app.config['SECRET_KEY'] = 'mysecretkey'


class InfoForm(FlaskForm):
    report = StringField('Enter your order')
    submit = SubmitField('Send')


@app.route('/', methods=["GET", "POST"])
def index():
    report = False
    form = InfoForm()
    if form.validate_on_submit():
        report = form.report.data
        form.report.data = ''
    return render_template('home.html', form=form, report=report)


if __name__ == '__main__':
    app.run()
